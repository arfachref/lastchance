import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user.service';
 
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponentUser implements OnInit {
  loginForm : FormGroup;
  isSubmitted = false;
  participateValue: boolean;
  constructor(private serviceUser: UserService, private toaster: ToastrService, private router: Router ) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email : new FormControl(null, [Validators.required]),
      password :  new FormControl(null, [Validators.required]),
    })
   }

  
   get formControls() { return this.loginForm.controls; }

  onSubmit(){
    console.log("form ", this.loginForm.value);
    this.isSubmitted = true;
    if(this.loginForm.invalid){
      return;
    }
    this.serviceUser.getUser().subscribe((data : any) => {
     let result =  data.filter(x => x.email == this.loginForm.value.email && x.password == this.loginForm.value.password );
      console.log("result: ",result);

      if(result.length == 0){
        this.toaster.error("Vérifier vos corrdonnées ")
      }else{
        this.router.navigate(['/event']);
      }
   
    })
  }

}
