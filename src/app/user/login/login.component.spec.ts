import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponentUser } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponentUser;
  let fixture: ComponentFixture<LoginComponentUser>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponentUser ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponentUser);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
