import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/services/event.service';
import {Event} from '../../model/Event';
import { Router } from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  events: Event[];
  eventName;
  participateValue : boolean = false;
  constructor(private _helper: EventService ,private router: Router) { }

  ngOnInit(): void {
    this._helper.getEvents().subscribe((data : any) =>  this.events = data);
  }

  participateEvent(e : Event){
   // this.participateValue = false;
    let i = this.events.indexOf(e);
    this.events[i].participate++;

  }
  logOut(){
    this.router.navigate(['/login'])
  }
}
