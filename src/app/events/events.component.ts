import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {Event} from '../model/Event';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  @Input() pChild: Event;
  @Output() participate = new EventEmitter<Event>();
  @Input() eventname: string;
  constructor() { }

  ngOnInit(): void {
    console.log(this.eventname)
  }

  participateClick(){
    this.participate.emit(this.pChild)
  }

}
