import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css']
})
export class AddEventComponent implements OnInit {
form: FormGroup;
  constructor(private _helper: EventService, private router: Router) {

    this.form = new FormGroup({
      title : new FormControl(null, [Validators.required]),
      description :  new FormControl(null, [Validators.required]),
    })
   }

 
  ngOnInit(): void {
  }

  save() {
    this._helper.addEvent(this.form.value).subscribe(
      () => {
        this.router.navigate(['/dashboard'])
      }
    );
  }

}
