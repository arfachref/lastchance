import { Component, OnInit } from '@angular/core';
import { Contact } from 'src/app/model/Contact';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contact : Contact[];
  constructor(private contactService : ContactService) { }

  ngOnInit(): void {
    this.contactService.getMessages().subscribe((data : any ) => {
      this.contact = data;
    });
  }

}
