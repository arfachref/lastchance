import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EventService } from 'src/app/services/event.service';
import {Event} from '../../model/Event';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  list: Event[];
  constructor(private _helper: EventService, private router: Router, private toaster: ToastrService) { }

  ngOnInit(): void {
    this._helper.getEvents().subscribe((data :any)=> this.list = data)
  }

  addEvent(){
    this.router.navigate(['/event/add'])
  }

  deleteEvent(e : Event){
    let i = this.list.indexOf(e);

    this._helper.delete(e.id).subscribe(data  => {

      this.list.splice(i, 1);
      this.toaster.success("Event Deleted with succès!");

    })
  }

  updateEvent(id){
    this.router.navigate(['/update', '2']);
  }
  logOut(){
    this.router.navigate(['/login'])
  }

}
