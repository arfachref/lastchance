import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/model/User';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm : FormGroup;
  isSubmitted = false;

  constructor(private _service: EventService, private toaster : ToastrService, private router : Router) {

    this.loginForm = new FormGroup({
      email : new FormControl(null, [Validators.required]),
      password :  new FormControl(null, [Validators.required]),
    })
   }

  ngOnInit(): void {
  }

  get formControls() { return this.loginForm.controls; }

  onSubmit(){
    console.log("form ", this.loginForm.value);
    this.isSubmitted = true;
    if(this.loginForm.invalid){
      return;
    }
    this._service.getAdmin().subscribe((data : any) => {
     let result =  data.filter(x => x.email == this.loginForm.value.email && x.password == this.loginForm.value.password );
      console.log("result: ",result);

      if(result.length == 0){
        this.toaster.error("Vérifier vos corrdonnées ")
      }else{
        this.router.navigate(['/dashboard']);
      }
   
    })
  }

}
