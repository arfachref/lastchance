import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EventService } from 'src/app/services/event.service';
import {Event } from '../../model/Event'
@Component({
  selector: 'app-update-event',
  templateUrl: './update-event.component.html',
  styleUrls: ['./update-event.component.css']
})
export class UpdateEventComponent implements OnInit {
  id;
  list;
  form : FormGroup
  constructor(private route: ActivatedRoute, private _helper: EventService,
     private toaster : ToastrService, private router: Router) { 
    this.form = new FormGroup({
      title : new FormControl(null, [Validators.required]),
      description :  new FormControl(null, [Validators.required]),
    })
   }
  

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id'); //this will fetch the query params
    this._helper.getEventsbyId(this.id).subscribe((data : any) => {
      this.form.setValue({title : data.title, description: data.description});
      //patchValue
    })
  }
  Back(){
    this.router.navigate(['/dashboard']);  
  }
  update(){
    this._helper.update(this.id, this.form.value).subscribe(data => {
      this.toaster.success("Event Updated with succès!");
      this.router.navigate(['/dashboard']);
    })
  }
}
