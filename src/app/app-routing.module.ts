import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './admin/login/login.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { AddEventComponent } from './admin/add-event/add-event.component';
import { UpdateEventComponent } from './admin/update-event/update-event.component';
import { ContactComponent } from './admin/contact/contact.component';
import { EventComponent } from './user/event/event.component';
import { LoginComponentUser } from './user/login/login.component';

const routes: Routes = [
  {
    path:'',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'event/add',
    component: AddEventComponent
  },
  {
    path: 'update/:id',
    component: UpdateEventComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'event',
    component: EventComponent
  },
  {
    path: 'user/login',
    component: LoginComponentUser
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]

})
export class AppRoutingModule { }
