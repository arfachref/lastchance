import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { User } from '../model/User';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  url = 'http://127.0.0.1:3000/admin';
  urlEvent = 'http://127.0.0.1:3000/events/';
  constructor(private http: HttpClient) {
  }

  getAdmin(){
    return this.http.get<User>(this.url);
  }

  getEvents(){
    return this.http.get<Event[]>(this.urlEvent);
  }

  getEventsbyId(id){
    return this.http.get<Event[]>(this.urlEvent + id);
  }
  addEvent(e: Event) {
    return this.http.post(this.urlEvent, e);
  }

  delete(id: number) {
    return this.http.delete(this.urlEvent + id);
  }
  update(id: number, e: Event) {
    return this.http.put(this.urlEvent + id, e);
  }



}