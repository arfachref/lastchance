import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { User } from "../model/User";

@Injectable({
  providedIn: 'root'
})
export class UserService { 
  url = 'http://127.0.0.1:3000/user';

  constructor(private http: HttpClient) {
  }

  getUser() {
    return this.http.get<User>(this.url);
  }

}