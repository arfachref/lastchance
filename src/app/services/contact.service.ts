import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Contact } from '../model/Contact';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  url = 'http://127.0.0.1:3000/contact';
  constructor(private http: HttpClient) {
  }

  getMessages(){
    return this.http.get<Contact[]>(this.url);
  }

  setMessage(c: Contact){
    return this.http.post(this.url, c);
  }

}

