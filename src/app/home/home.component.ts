import { Component, OnInit } from '@angular/core';
import { EventService } from '../services/event.service';
import {Event} from '../model/Event';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ContactService } from '../services/contact.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  images = [1, 2, 3, 7, 5, 6].map((n) => `assets/images/portfolio/${n}.jpg`);
  events: Event[];
  formContact: FormGroup

  constructor(private _helper: EventService,
     private contactService : ContactService, private toaster : ToastrService, private router: Router ) {
    this.formContact = new FormGroup({
      name : new FormControl(null, [Validators.required]),
      email : new FormControl(null, [Validators.required]),
      message : new FormControl(null, [Validators.required]),
      phone : new FormControl(null, [Validators.required]),

    })
   }

  ngOnInit(): void {
    this._helper.getEvents().subscribe((data : any) =>  this.events = data);
  }

  participateEvent(e: Event){
    //let i = this.list.indexOf(e);
    console.log("participateEvent");
    this.router.navigate(['/user/login']);
  } 

  sendMessage(){
    this.contactService.setMessage(this.formContact.value).subscribe((data => {
      this.toaster.success("Message envoyé avec succès")
    }))
  }

}
