import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { EventsComponent } from './events/events.component';
import { LoginComponent } from './admin/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddEventComponent } from './admin/add-event/add-event.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { UpdateEventComponent } from './admin/update-event/update-event.component';
import { ContactComponent } from './admin/contact/contact.component';
import { EventComponent } from './user/event/event.component';
import { UserService } from './services/user.service';
import { LoginComponentUser } from './user/login/login.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EventsComponent,
    LoginComponent,
    DashboardComponent,
    AddEventComponent,
    UpdateEventComponent,
    ContactComponent,
    EventComponent,
    LoginComponentUser,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule ,
    BrowserAnimationsModule,
    ToastrModule.forRoot({ positionClass: 'toast-top-right' }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
